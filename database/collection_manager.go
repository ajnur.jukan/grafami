package database

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	USERS                       = "users"
	CALLS                       = "calls"
	collectionAlreadyExistsCode = 48
)

func createCollections(database *mongo.Database) error {
	collectionOptions := options.CreateCollection()
	collections := getCollectionNames()

	for _, collectionName := range collections {
		err := database.CreateCollection(context.Background(), collectionName, collectionOptions)

		// Check if the error is due to a duplicate collection name (collection already created)
		collectionExists := true
		if err != nil {
			commandErr, ok := err.(mongo.CommandError)
			if ok {
				collectionExists = commandErr.HasErrorCode(collectionAlreadyExistsCode)
			}
		}

		if err != nil && collectionExists {
			// 48 is the MongoDB error code for NamespaceExists
			// Ignore the error, as it indicates that the collection already exists
			fmt.Printf("Collection '%s' already exists, skipping creation.\n", collectionName)
			continue
		} else if err != nil {
			return err
		}
	}

	return nil
}

func getCollectionNames() []string {
	return []string{USERS, CALLS}
}
