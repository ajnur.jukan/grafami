package database

import (
	"context"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func ConnectAndSetup(mongoURL string) (*mongo.Client, error) {
	// Use the SetServerAPIOptions() method to set the Stable API version to 1
	serverAPI := options.ServerAPI(options.ServerAPIVersion1)
	opts := options.Client().ApplyURI(mongoURL).SetServerAPIOptions(serverAPI)
	// Create a new client and connect to the server
	dbClient, err := mongo.Connect(context.TODO(), opts)
	if err != nil {
		return nil, err
	}

	err = createCollections(dbClient.Database("admin"))
	if err != nil {
		fmt.Println("evo me ovdje")
		log.Fatal(err)
	}

	// Send a ping to confirm a successful connection
	var result bson.M
	if err := dbClient.Database("admin").RunCommand(context.TODO(), bson.D{{Key: "ping", Value: 1}}).Decode(&result); err != nil {
		return nil, err
	}
	log.Println("Pinged your database deployment. You successfully connected to MongoDB!")

	usersCollection := dbClient.Database("admin").Collection("users")

	// Define the index model
	index := mongo.IndexModel{
		Keys:    bson.M{"username": 1},
		Options: options.Index().SetUnique(true),
	}

	// Create the index
	if _, err := usersCollection.Indexes().CreateOne(context.Background(), index); err != nil {
		return nil, err
	}

	return dbClient, nil
}
