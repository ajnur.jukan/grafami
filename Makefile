all: clean build

build:
	go build -o monami

clean:
	go clean -x

test:
	go clean -testcache && richgo test -v -short -count=1 ./...

test-race:
	go clean -testcache && richgo test -v -short -race -count=1 ./...

coverage:
	go clean -testcache && go test -short -coverprofile=coverage.out ./... && go tool cover -html=coverage.out && rm coverage.out