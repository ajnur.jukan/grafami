package amigo

import (
	"fmt"
	"net/http"

	websocket "github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	WriteBufferSize: 1024,
	ReadBufferSize:  1024,
}

// AMIData pulls important data from Amigo structure and prepares it for sending
func (ami *Amigo) AMIData() *SetupData {
	data := &SetupData{
		RegisteredDevices: len(ami.Endpoints),
		ActiveDevices:     ami.Active,
		DeviceList:        make([]Device, 0, len(ami.Endpoints)),
		BridgeCount:       ami.Bridges,
		EndpointNames:     ami.CallerIds,
	}

	for id, status := range ami.Endpoints {
		ext := extractExtension(id) // I get here number from PJSIP/104 , ext is 104
		callerID := findCallerID(ext, ami.CallerIds)
		if callerID == "" {
			callerID = id
		}
		data.DeviceList = append(data.DeviceList, Device{ID: id, Status: status, Name: callerID})
	}

	return data
}

// ConnectClient upgrades a client which has request to connect to us to a websocket and sends page initialization information through it
func (ami *Amigo) ConnectClient(w http.ResponseWriter, r *http.Request) {
	upgrader.CheckOrigin = func(r *http.Request) bool { return true } // CORS is unimportant here, accept all requests
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	if err := ws.WriteJSON(&Message{Type: "setup", Data: ami.AMIData()}); err != nil {
		fmt.Println(err.Error())
		return
	} // send setup information to connected client and check for errors

	ami.Hub.AddClient(ws) // add the client to our websocket hub
}

// StartWS starts listening to incoming connection requests
func (ami *Amigo) StartWS(addr string) {
	http.HandleFunc("/", ami.ConnectClient)
	go http.ListenAndServe(addr, nil)
	ami.Hub.Start()
}
