package amigo

type CallInfo struct {
	Channel      string `json:"channel"`
	CallId       string `json:"call_id"`
	CallerName   string `json:"caller_name"`
	CallerNumber string `json:"caller_number"`
	CalleeName   string `json:"callee_name"`
	CalleeNumber string `json:"callee_number"`
	StartedAt    int64  `json:"started_at"`
	FinishedAt   int64  `json:"finished_at"`
}

type Message struct {
	Type string `json:"type"`
	Data any    `json:"data"`
}
type SetupData struct {
	RegisteredDevices int      `json:"regdev"`
	ActiveDevices     int      `json:"activedev"`
	DeviceList        []Device `json:"devicelist"`
	BridgeCount       int      `json:"bridgecount"`
	EndpointNames     []string `json:"endpointnames"`
}

type Device struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	Status string `json:"status"`
}
