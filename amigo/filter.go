package amigo

import (
	"log"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"
)

// trimInfo is used to pull data from responses such as "Event: <event name>". It would take event name and return it to us.
func trimInfo(line string) string {
	return strings.Trim(strings.Split(line, " ")[1], "\r\n")
}

// a simple way to check if we have a function defined for an event and call it if we do
var filter = map[string]func([]string, *Amigo){
	"SuccessfulAuth":    succAuth,
	"DeviceStateChange": devStateChange,
	"BridgeCreate":      addBridge,
	"BridgeEnter":       callDetected,
	"BridgeDestroy":     rmBridge,
	"Hangup":            callHangup,
	"Status":            callDetected,
}

var callMutex = sync.Mutex{}
var calls = make(map[string]*CallInfo, 0)

func succAuth(event []string, ami *Amigo) {
	dev, ip := trimInfo(event[6]), trimInfo(event[9])
	log.Printf("Successful Authentication by %s from %s\n", dev, ip)

	ami.Hub.Broadcast <- Message{ // send data to websockets connected to us
		Type: "succauth",
		Data: []string{dev, ip},
	}
}

func devStateChange(event []string, ami *Amigo) {
	dev, state := trimInfo(event[2]), trimInfo(event[3])
	prevstate := ami.Endpoints[dev]
	ami.Endpoints[dev] = state
	log.Printf("%s is now %s", dev, state)

	if prevstate == "UNAVAILABLE" {
		ami.Active++
	}
	if state == "UNAVAILABLE" {
		ami.Active--
	} // previous state hos to be checked to determine the amount of active endpoints

	ami.Hub.Broadcast <- Message{
		Type: "devstatechange",
		Data: []string{dev, ami.Endpoints[dev]},
	}

	ami.Hub.Broadcast <- Message{
		Type: "activedevs",
		Data: ami.Active,
	}
}

func addBridge(event []string, ami *Amigo) {
	ami.Bridges++
	log.Println("Bridge created")

	ami.Hub.Broadcast <- Message{
		Type: "brcountupdate",
		Data: []string{"Bridge created", strconv.Itoa(ami.Bridges)},
	}
}

func callDetected(event []string, ami *Amigo) {
	log.Println("Call detected")
	callInfo := getCallInfo(event)

	callMutex.Lock()
	defer callMutex.Unlock()
	_, exists := calls[callInfo.CallId]
	if exists {
		return
	}

	callInfo.StartedAt = time.Now().Unix()
	calls[callInfo.CallId] = callInfo

	ami.Hub.Broadcast <- Message{
		Type: "call_answered",
		Data: callInfo,
	}
}

func callHangup(event []string, ami *Amigo) {
	callMutex.Lock()
	defer callMutex.Unlock()

	log.Println("Call finished")
	callId := getHangupedCallId(event)
	if callId == "" {
		log.Println("Failed to get callid on call hangup event")
	} else {
		callInfo := calls[callId]
		if ami.db != nil && callInfo != nil {
			ami.db.StoreCallInfoOnHangup(callInfo)
		}
	}

	delete(calls, callId)

	ami.Hub.Broadcast <- Message{
		Type: "call_hangup",
		Data: callId,
	}
}

func rmBridge(event []string, ami *Amigo) {
	ami.Bridges--
	log.Println("Bridge destroyed")

	ami.Hub.Broadcast <- Message{
		Type: "brcountupdate",
		Data: []string{"Bridge destroyed", strconv.Itoa(ami.Bridges)},
	}
}

func getCallInfo(event []string) *CallInfo {
	// Define regular expressions
	callerIDNumRegex := regexp.MustCompile(`CallerIDNum: (\d+)`)
	callerIDNameRegex := regexp.MustCompile(`CallerIDName: ([^\n]+)`)
	connectedLineNumRegex := regexp.MustCompile(`ConnectedLineNum: (\d+)`)
	connectedLineNameRegex := regexp.MustCompile(`ConnectedLineName: ([^\n]+)`)
	channelIdRegex := regexp.MustCompile(`Parentid: ([^\n]+)`)
	pjsipChannelNameRegex := regexp.MustCompile(`Channel: ([^\n]+)`)

	callInfo := &CallInfo{}

	for _, ev := range event {
		// Find matches in the log event
		callerIDNumMatches := callerIDNumRegex.FindStringSubmatch(ev)
		callerIDNameMatches := callerIDNameRegex.FindStringSubmatch(ev)
		connectedLineNumMatches := connectedLineNumRegex.FindStringSubmatch(ev)
		connectedLineNameMatches := connectedLineNameRegex.FindStringSubmatch(ev)
		channelIdRegexMatches := channelIdRegex.FindStringSubmatch(ev)
		pjsipChannelNameRegexMatches := pjsipChannelNameRegex.FindStringSubmatch(ev)

		if len(callerIDNumMatches) > 1 {
			callInfo.CallerNumber = callerIDNumMatches[1]
		}

		if len(callerIDNameMatches) > 1 {
			callInfo.CallerName = callerIDNameMatches[1]
		}

		if len(connectedLineNumMatches) > 1 {
			callInfo.CalleeNumber = connectedLineNumMatches[1]
		}

		if len(connectedLineNameMatches) > 1 {
			callInfo.CalleeName = connectedLineNameMatches[1]
		}

		if len(channelIdRegexMatches) > 1 {
			callInfo.CallId = channelIdRegexMatches[1]
		}

		if len(pjsipChannelNameRegexMatches) > 1 {
			callInfo.Channel = pjsipChannelNameRegexMatches[1]
		}
	}

	return callInfo
}

func getHangupedCallId(event []string) string {
	channelIdRegex := regexp.MustCompile(`Parentid: ([^\n]+)`)
	var callId string
	for _, ev := range event {
		channelIdRegexMatches := channelIdRegex.FindStringSubmatch(ev)
		if len(channelIdRegexMatches) > 1 {
			callId = channelIdRegexMatches[1]
			break
		}
	}

	return callId
}

func GetActiveCalls() map[string]*CallInfo {
	callMutex.Lock()
	defer callMutex.Unlock()

	return calls
}

func extractExtension(s string) string {
	parts := strings.Split(s, "/")
	if len(parts) == 2 {
		return parts[1]
	}
	return ""
}

// Helper function to find Caller ID for a given extension
func findCallerID(extension string, callerIDs []string) string {
	for _, cid := range callerIDs {
		if strings.Contains(cid, extension) {
			return cid
		}
	}
	return ""
}
