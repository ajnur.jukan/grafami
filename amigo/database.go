package amigo

import (
	"context"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
)

type DatabaseController struct {
	Driver *mongo.Client
}

type Database interface {
	StoreCallInfoOnHangup(*CallInfo) error
}

func (db *DatabaseController) StoreCallInfoOnHangup(ci *CallInfo) error {
	adminDatabase := db.Driver.Database("admin")
	callsCollection := adminDatabase.Collection("calls")

	ci.FinishedAt = time.Now().Unix()

	_, err := callsCollection.InsertOne(context.TODO(), ci)
	if err != nil {
		log.Println(err)
		return err
	}

	log.Printf("Call %s added in database", ci.CallId)
	return nil
}
