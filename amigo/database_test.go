package amigo

import (
	"context"
	"log"
	"monami/database"
	"testing"

	"github.com/stretchr/testify/suite"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

const mongoDbTestURL = "mongodb://localhost:27018"

type AMIgoDatabaseTestSuite struct {
	suite.Suite
	db                *mongo.Client
	amigoDbController *DatabaseController
}

func TestAmigoDatabaseTestSuite(t *testing.T) {
	suite.Run(t, new(AMIgoDatabaseTestSuite))
}

func (suite *AMIgoDatabaseTestSuite) SetupSuite() {
	dbClient, err := database.ConnectAndSetup(mongoDbTestURL)
	if err != nil {
		log.Fatal(err)
	}

	adminDatabase := dbClient.Database("admin")
	callsCollection := adminDatabase.Collection("calls")

	_, err = callsCollection.DeleteMany(context.TODO(), bson.D{})
	suite.Equal(nil, err)

	suite.db = dbClient
	suite.amigoDbController = &DatabaseController{Driver: dbClient}
}

func (suite *AMIgoDatabaseTestSuite) TearDownSuite() {
	suite.db.Disconnect(context.TODO())
}

func (suite *AMIgoDatabaseTestSuite) TestStoreCallInfoAfterHangup() {
	adminDatabase := suite.db.Database("admin")
	callsCollection := adminDatabase.Collection("calls")

	callInfo := &CallInfo{
		Channel:      "PJSIP/105-00000013",
		CallId:       "1702025671.18",
		CallerName:   "Ajnur Jukan",
		CalleeName:   "Emir Meskovic",
		CallerNumber: "061843933",
		CalleeNumber: "062872312",
		StartedAt:    1702025674,
	}

	err := suite.amigoDbController.StoreCallInfoOnHangup(callInfo)
	suite.Equal(nil, err)

	count, err := callsCollection.CountDocuments(context.Background(), bson.M{})
	suite.Equal(nil, err)
	suite.Equal(int64(1), count)
}
