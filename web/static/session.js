function isUserSessionValid(sessionData) {
	return new Promise((resolve, reject) => {
		fetch('http://127.0.0.1:20000/validate_session', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify(sessionData)
			})
			.then(response => {
				if (response.ok) {
					resolve(true);
				} else {
					resolve(false);
				}
			})
			.catch(error => {
				resolve(false);
			});
	});
}

function handleLogin(loginData) {
	return fetch('http://127.0.0.1:20000/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(loginData)
		})
		.then(response => {
			if (!response.ok) {
				throw new Error(`HTTP error! Status: ${response.status}`);
			}
			return response.json();
		});
}

function handleLogout(sessionData) {
	fetch('http://127.0.0.1:20000/logout', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(sessionData)
		})
		.then(response => {
			//even if something fails move user to the login page
			window.location.href = 'login.html';
		})
}

function checkUserSessionAndRedirect(pageName) {
	var data = {
		token: getAuthTokenCookie(),
	};

	isUserSessionValid(data)
		.then(validSession => {
			if (!validSession) {
				window.location.href = pageName;
			}
		});
}