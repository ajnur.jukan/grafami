let socket = null;

const statusColor = {
	UNKNOWN: 'gray',
	NOT_INUSE: 'green',
	INUSE: 'blue',
	BUSY: 'red',
	INVALID: 'purple',
	UNAVAILABLE: 'orange',
	RINGING: 'yellow',
	RINGINUSE: 'orange',
	ONHOLD: 'lightblue',
};

document.addEventListener("DOMContentLoaded", function () {
	socket = new WebSocket("ws://localhost:9999/");
	socket.onopen = (data) => {
		console.log("Websocket connection established")
	}
	socket.onclose = () => {
		console.log("Websocket connection closed")
	}
	socket.onerror = error => {
		console.log(error)
	}
	socket.onmessage = msg => {
		let jmsg = JSON.parse(msg.data)
		switch (jmsg.type) {
			case "setup":
				setup(jmsg.data)
				break;
			case "call_hangup":
				removeCallRowAfterHangup(jmsg.data);
				break;
			case "call_answered":
				callAnswered(jmsg.data);
				break;
			case "brcountupdate":
				document.getElementById("chans").innerHTML = jmsg.data[1]
				addEvent(jmsg.data[0], "exchange")
				break;
			case "activedevs":
				document.getElementById("devs").innerHTML = jmsg.data
				break;
			case "devstatechange":
				var state = document.getElementById(`devstate_${jmsg.data[0]}`)
				state.innerHTML = jmsg.data[1];
				var classes = Array.from(state.classList);
				classes.pop();
				classes.push(statusColor[jmsg.data[1]])
				state.className = classes.join(' ');
				addEvent(`<a> ${jmsg.data[0]} </a> is now <a> ${jmsg.data[1]} </a>`, "phone")
				break;
			case "succauth":
				addEvent(`Successful authentication by <a>${jmsg.data[0]}</a> from <a>${jmsg.data[1]}</a>`, "key")
				break;
			default:
				console.log("ERROR: unrecognized request")
		}
	}
})

function setup(data) {
	const template = document.getElementById('device')
	const devlist = document.getElementById('devlist')

	document.getElementById('chans').innerHTML = data.bridgecount

	var activeDevices = 0;
	for (let index = 0; index < data.devicelist.length; index++) {
		const element = data.devicelist[index];
		if (element.id.startsWith("PJSIP/") == false) {
			continue
		}

		if (element.status != "UNAVAILABLE") {
			activeDevices++;
		}

		instance = document.importNode(template.content, true);

		var devName = instance.querySelector('.ui.left.aligned.segment');
		var devStat = instance.querySelector('.ui.inverted.secondary.segment');
		devName.id = 'devname_' + element.id;
		devStat.id = 'devstate_' + element.id;

		devName.innerHTML = element.name;
		devStat.innerHTML = element.status;

		devStat.classList.add(statusColor[element.status])

		devlist.appendChild(instance);
	}

	document.getElementById('devs').innerHTML = activeDevices;

	fetch('http://127.0.0.1:20000/active_calls')
		.then(response => {
			if (!response.ok) {
				throw new Error('Network response was not ok');
			}
			return response.json();
		})
		.then(data => {
			for (const key in data) {
				if (data.hasOwnProperty(key)) {
					callAnswered(data[key]);
				}
			}
		})
		.catch(error => {
			console.error('Error fetching data:', error);
		});
}

function addEvent(message, icon) {
	const template = document.getElementById('event');
	const feed = document.getElementById('feed');

	const date = new Date();
	const instance = document.importNode(template.content, true);
	instance.querySelector('.date').innerHTML = date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
	instance.querySelector('.summary').innerHTML = message;

	instance.querySelector('.label').innerHTML = `<i class="${icon} tiny icon inverted"></i>`;

	feed.insertBefore(instance, feed.firstChild)
}

function removeCallRowAfterHangup(callId) {
	// Find the row with the specified call ID
	var rowToRemove = document.getElementById('callRow_' + callId);
	if (rowToRemove) {
		// Remove the row if found
		rowToRemove.remove();
	}
}

function openConfirmationModal(callInfo) {
	$('#hangupConfirmationModal .content').text(`Are you sure you want to hang up the call between ${callInfo.caller_name} and ${callInfo.callee_name} ?`);
	$('#hangupConfirmationModal').data('callId', callInfo.call_id.trim());
	$('#hangupConfirmationModal').data('channel', callInfo.channel.trim());
	$('#hangupConfirmationModal').modal('show');
}

function hangupCall(callInfo) {
	// Open the confirmation modal
	openConfirmationModal(callInfo);
}

function callAnswered(callInfo) {
	const isAdmin = getAdminCookie();

	var template = document.getElementById('callRow');
	var clone = document.importNode(template.content, true);

	// Set the call information in the template
	var summary = `${callInfo.caller_name.trim()} (${callInfo.caller_number}) - ${callInfo.callee_name.trim()} (${callInfo.callee_number})`;
	clone.querySelector('.summary').textContent = summary;
	var rowId = 'callRow_' + callInfo.call_id;
	clone.querySelector('.callRow').id = rowId;

	if (!isAdmin) {
		clone.querySelector('.action').remove();
	} else {
		// If user is admin, add click event to the hangup button
		var hangupButton = clone.querySelector('.hangupButton');
		hangupButton.addEventListener('click', function () {
			hangupCall(callInfo);
		});
	}

	// Append the template to the call list
	document.getElementById('callList').appendChild(clone);
}