// Function to get a cookie value by name
function getCookie(name) {
	const value = `; ${document.cookie}`;
	const parts = value.split(`; ${name}=`);
	if (parts.length === 2) {
		return parts.pop().split(';').shift();
	}
}

function getAuthTokenCookie() {
	return getCookie("authToken")
}

function getAdminCookie() {
	return getCookie("admin") == "true"
}