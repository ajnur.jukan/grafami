$(document).ready(function () {
	isAdmin = getAdminCookie();
	if (!isAdmin) {
		// Remove the "Action" column header
		$('#userTable thead tr th:last-child').remove();
		$('#roleField').hide();
	} else {
		var roleField = $('select[name="role"]');
		roleField.prop('required', isAdmin);
	}

	loadUsers();
});

$('#addUserButton').click(function () {
	$('#addUserModal').modal('show');
});

//Adding a click event to the "Yes" button in the delete confirmation modal
$('.ok.button').click(function () {
	// Retrieve the username from the data attribute
	var username = $('#deleteConfirmationModal .ok.button').data('username');
	deleteUser(username);

	// Close the delete confirmation modal
	$('#deleteConfirmationModal').modal('hide');
});

$(document).on('click', '.update-button', function () {
	// Retrieve the username from the data attribute
	var username = $(this).data('username');
	handleUpdateButtonClick(username);
});

$('#addUserForm').submit(function (event) {
	event.preventDefault();

	var formData = {
		name: $('input[name="name"]').val(),
		surname: $('input[name="surname"]').val(),
		username: $('input[name="username"]').val(),
		password: $('input[name="password"]').val()
	};

	if (!isAdmin) {
		formData.role = 'user';
	} else {
		// Read role from the dropdown select
		formData.role = $('select[name="role"]').val();
	}


	// Validate the username format
	if (!isValidUsername(formData.username)) {
		$('#usernameErrorMessage').text('Username must be in the format test@gmail.com');
		$('#usernameErrorModal').modal('show');
		return;
	}

	$('#addUserModal').modal('hide');

	addUser(formData, false);
});

$('#updateUserForm').submit(function (event) {
	event.preventDefault();

	var formData = {
		name: $('input[name="update_name"]').val(),
		surname: $('input[name="update_surname"]').val(),
		username: $('input[name="update_username"]').val(),
		role: $('select[name="update_role"]').val(),
		password: $('input[name="update_password"]').val()
	};

	$('#updateUserModal').modal('hide');

	addUser(formData, true);
});

function addUser(userData, isUpdate) {
	event.preventDefault(); // Prevent the default form submission
	method = "POST";
	if (isUpdate) {
		method = "PATCH";
	}

	// Make an AJAX request to add the user
	$.ajax({
		url: 'http://127.0.0.1:20000/users', // Replace with your API endpoint
		type: method,
		contentType: 'application/json',
		data: JSON.stringify(userData),
		success: function (data, status, xhr) {
			if (xhr.status === 200) {
				// Load users again after adding the user
				location.reload();
			} else {
				showErrorModal(
					'Error! Username already exist or internal server error happened!');
			}
		},
		error: function (xhr, status, error) {
			showErrorModal(
				'Error! Username already exist or internal server error happened!');
		}
	});
}

function handleUpdateButtonClick(username) {
	// Retrieve user details from the table row
	var userDetails = getUserDetailsFromTableRow(username);

	// Populate the update modal fields
	$('#updateUserForm input[name="update_name"]').val(userDetails.name);
	$('#updateUserForm input[name="update_surname"]').val(userDetails.surname);
	$('#updateUserForm input[name="update_username"]').val(userDetails.username);
	$('#updateUserForm select[name="update_role"]').val(userDetails.role);

	// Show the update modal
	$('#updateUserModal').modal('show');
}

function confirmDelete(name, username) {
	$('#deleteUserName').text(name);
	$('#deleteConfirmationModal .ok.button').data('username', username);
	$('#deleteConfirmationModal').modal('show');
}

// Function to get user details directly from the table row
function getUserDetailsFromTableRow(username) {
	var userDetails = {};
	var row = $(`#userTable tbody tr:has(td:contains('${username}'))`);

	if (row.length > 0) {
		userDetails.name = row.find('td:eq(1)').text();
		userDetails.surname = row.find('td:eq(2)').text();
		userDetails.username = row.find('td:eq(0)').text();
		userDetails.role = row.find('td:eq(3)').text();
	}

	return userDetails;
}

function loadUsers() {
	isAdmin = getAdminCookie();
	var columnDefs = [];
	if (isAdmin) {
		// Add the column definition only if the column exists
		columnDefs.push({
			targets: [5],
			orderable: false
		});
	}
	columnDefs.push({
		targets: [4],
		visible: false
	});

	$.ajax({
		url: 'http://127.0.0.1:20000/users?token=' + getAuthTokenCookie(),
		type: 'GET',
		success: function (users) {
			displayUsers(users);

			$('#userTable').DataTable({
				paging: false, // Disable paging
				searching: true, // Enable searching
				order: [
					[4, 'asc']
				], // Initial sorting by created date_time in ascending order
				language: {
					info: 'Total: _TOTAL_ entries'
				},
				columnDefs: columnDefs,
				dom: "<'ui grid'<'row'<'eight wide column'l><'eight wide column'f>>r>t<'ui grid'<'row'<'eight wide column'i><'eight wide column'p>>>",

			});
		},
		error: function (error) {
			showErrorModal('Error loading user!');
		}
	});
}

function displayUsers(users) {
	var tbody = $('#userTable tbody');
	tbody.empty(); // Clear existing rows
	users = JSON.parse(users);

	if (!users) {
		return;
	}

	users.forEach(function (user) {
		var row = $('<tr>');
		row.append($('<td>').text(user.username));
		row.append($('<td>').text(user.name));
		row.append($('<td>').text(user.surname));
		row.append($('<td>').text(user.role));
		row.append($('<td>').text(user.createdtime));

		isAdmin = getAdminCookie();
		if (isAdmin && user.username !== 'admin') {
			row.append($('<td>').html(
				`<button class="ui blue button update-button" id="update-button" data-username="${user.username}">Update</button>
				<button class="ui red button" onclick="confirmDelete('${user.name} ${user.surname}', '${user.username}')">Delete</button>`
			));
		}

		tbody.append(row);
	});
}

function deleteUser(username) {
	// Assuming you have an API endpoint for deleting users
	var deleteUrl = 'http://127.0.0.1:20000/users';

	// Make a DELETE request to delete the user
	$.ajax({
		url: deleteUrl,
		type: 'DELETE',
		contentType: 'application/json',
		data: JSON.stringify({
			username: username
		}), // Send username in the request body
		success: function (data, status, xhr) {
			if (xhr.status === 200) {
				// If delete is successful (status code 200 OK), fetch the updated user list
				location.reload();
			} else {
				showErrorModal('Failed to delete user');
			}
		},
		error: function (xhr, status, error) {
			showErrorModal('Failed to delete user!');
		}
	});
}

function goBack() {
	window.location.href = 'dashboard.html';
}

function isValidUsername(username) {
	var usernameRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/;
	return usernameRegex.test(username);
}

function showErrorModal(message) {
	$('#errorMessage').text(message);
	$('#errorModal').modal('show');
}