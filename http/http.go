package http

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	"monami/amigo"

	"github.com/gorilla/mux"
)

type Http struct {
	httpRouter *mux.Router
	httpServer *http.Server
	db         Database
	ami        *amigo.Amigo
}

// initalizesNewHttpInstance
func NewHttpInstance(database Database, ami *amigo.Amigo) *Http {
	http := &Http{
		httpRouter: mux.NewRouter(),
		httpServer: &http.Server{
			Addr:         fmt.Sprintf("%s:%d", "127.0.0.1", 20000),
			Handler:      nil,
			ReadTimeout:  10 * time.Second,
			WriteTimeout: 300 * time.Second,
		},
		db:  database,
		ami: ami,
	}
	http.httpServer.SetKeepAlivesEnabled(false)

	http.httpRouter.Use(handleCORS)

	return http
}

// InitializeHTTPRouter registers http handlers
func (h *Http) InitializeHTTPRouter() {
	h.httpRouter.HandleFunc("/login", func(w http.ResponseWriter, r *http.Request) {
		handleUserLogin(w, r, h.db)
	})

	h.httpRouter.HandleFunc("/logout", func(w http.ResponseWriter, r *http.Request) {
		handleUserLogout(w, r, h.db)
	})

	h.httpRouter.HandleFunc("/validate_session", func(w http.ResponseWriter, r *http.Request) {
		handleValidateSession(w, r, h.db)
	})

	h.httpRouter.HandleFunc("/users", func(w http.ResponseWriter, r *http.Request) {
		handleUsersRequest(w, r, h.db)
	})

	h.httpRouter.HandleFunc("/call_hangup", func(w http.ResponseWriter, r *http.Request) {
		handleCallHangup(w, r, h.ami)
	})

	h.httpRouter.HandleFunc("/active_calls", func(w http.ResponseWriter, r *http.Request) {
		handleGetActiveCalls(w, r)
	})

	h.httpRouter.HandleFunc("/call_history", func(w http.ResponseWriter, r *http.Request) {
		handleGetCallHistory(w, r, h.db)
	})

	h.httpRouter.HandleFunc("/csv_call_history", func(w http.ResponseWriter, r *http.Request) {
		handleCsvGetCallHistory(w, r, h.db)
	})

	h.httpServer.Handler = h.httpRouter
}

// Starts HTTP server to listen and serve in a separate goroutine for future http requests
func (h *Http) StartHttp() {
	log.Print("Starting HTTP server...")
	go func() {
		if err := h.httpServer.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("HTTP server cannot be started. Error %v... Shutting down superheroes app...", err)
		}
	}()
}

// StopHttp stops http server with possible
// of 5 seconds delay to handle request that are being parsed currently
func (h *Http) StopHttp() {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := h.httpServer.Shutdown(ctx); err != nil {
		log.Printf("Failed to shutdown http server successfully: %v", err)
		return
	}

	log.Print("HTTP server stopped successfully")
}

func handleCORS(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Set CORS headers for all responses
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PATCH, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With")

		// Handle preflight OPTIONS requests
		if r.Method == http.MethodOptions {
			w.WriteHeader(http.StatusOK)
			return
		}

		// Continue with the next handler in the chain
		next.ServeHTTP(w, r)
	})
}
