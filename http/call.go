package http

import (
	"encoding/json"
	"log"
	amigo "monami/amigo"
	"net/http"
	"strconv"
	"time"
)

func handleCallHangup(w http.ResponseWriter, r *http.Request, ami *amigo.Amigo) {
	log.Println("Handling call hangup request...")

	call := &Call{}
	if err := json.NewDecoder(r.Body).Decode(call); err != nil {
		log.Println("Failed to decode http request inside call hangup handler")
		http.Error(w, "Failed to handle logout request", http.StatusBadRequest)
		return
	}

	err := ami.CallHangup(call.Channel, call.CallId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func handleGetActiveCalls(w http.ResponseWriter, r *http.Request) {
	log.Println("Handling get active calls request...")

	activeCalls := amigo.GetActiveCalls()
	activeCallsJS, _ := json.Marshal(activeCalls)
	w.Write(activeCallsJS)
}

func handleGetCallHistory(w http.ResponseWriter, r *http.Request, db Database) {
	log.Println("Handling get call history request...")

	var offset int
	page, err := strconv.Atoi(r.URL.Query().Get("page"))
	if err == nil && page > 1 {
		offset = (page - 1) * 10
	}

	filters := getFiltersForCallHistory(r)
	filters.Limited = true
	filters.Offset = offset

	calls, err := db.GetCallHistory(filters)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	callsJS, _ := json.Marshal(calls)
	w.Write(callsJS)
}

func handleCsvGetCallHistory(w http.ResponseWriter, r *http.Request, db Database) {
	log.Println("Handling get csv call history request...")

	filters := getFiltersForCallHistory(r)
	filters.Limited = false

	calls, err := db.GetCallHistory(filters)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	callsJS, _ := json.Marshal(calls)
	w.Write(callsJS)
}

func getFiltersForCallHistory(r *http.Request) *Filter {
	filter := &Filter{}
	from, err := strconv.Atoi(r.URL.Query().Get("from"))
	if err != nil {
		from = 0
	} else {
		filter.From = int64(from)
	}

	to, err := strconv.Atoi(r.URL.Query().Get("to"))
	if err != nil {
		filter.To = time.Now().Unix()
	} else {
		filter.To = int64(to)
	}

	filter.SearchBy = r.URL.Query().Get("search")
	return filter
}
