package http

import (
	"context"
	"errors"
	"log"
	amigo "monami/amigo"
	"strings"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	ADMIN_USERNAME = "admin"
)

func (db *DatabaseController) CanLogIn(username, password string) bool {
	adminDatabase := db.Driver.Database("admin")
	usersCollection := adminDatabase.Collection("users")

	filter := bson.M{"username": username, "password": password}

	// Find the documents that match the query
	cursor, err := usersCollection.Find(context.Background(), filter)
	if err != nil {
		log.Println(err)
		return false
	}
	defer cursor.Close(context.Background())

	// Iterate over the cursor and print the documents
	for cursor.Next(context.Background()) {
		var document bson.M
		if err := cursor.Decode(&document); err != nil {
			log.Println(err)
			return false
		}

		if username == document["username"].(string) && document["password"].(string) == password {
			log.Println("Login successfull")
			return true
		}
	}

	if err := cursor.Err(); err != nil {
		log.Println(err)
		return false
	}

	return false
}

func (db *DatabaseController) SetUserToken(token, username string) bool {
	adminDatabase := db.Driver.Database("admin")
	usersCollection := adminDatabase.Collection("users")

	// Create a filter to find the document with the specified username
	filter := bson.M{"username": username}

	// Create an update to add the new key to the document
	update := bson.M{"$set": bson.M{"token": token}}

	// Use UpdateOne to apply the update to a single document
	_, err := usersCollection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		log.Println(err)
		return false
	}

	return true
}

func (db *DatabaseController) RemoveUserToken(token string) bool {
	adminDatabase := db.Driver.Database("admin")
	usersCollection := adminDatabase.Collection("users")

	// Create a filter to find the document with the specified token
	filter := bson.D{{Key: "token", Value: bson.D{{Key: "$eq", Value: token}}}}

	// Create an update to set the token field to an empty string
	update := bson.D{{Key: "$set", Value: bson.D{{Key: "token", Value: ""}}}}

	// Perform the update operation for the document matching the filter
	_, err := usersCollection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		log.Println(err)
		return false
	}

	return true
}

func (db *DatabaseController) IsSessionValid(token string) bool {
	if token == "" {
		log.Println("Empty token recieved while validating session. Session not valid!")
		return false
	}

	adminDatabase := db.Driver.Database("admin")
	usersCollection := adminDatabase.Collection("users")

	filter := bson.M{"token": token}
	// Use CountDocuments to get the count of documents that match the filter
	count, err := usersCollection.CountDocuments(context.Background(), filter)
	if err != nil {
		log.Println(err)
		return false
	}

	return count > 0
}

func (db *DatabaseController) GetUsers(requesterToken string) ([]*User, error) {
	isRequesterAdmin := db.IsAdmin(requesterToken)

	adminDatabase := db.Driver.Database("admin")
	usersCollection := adminDatabase.Collection("users")

	// Set a timeout for the connection
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	opts := options.Find().SetSort(map[string]int{"createdtime": -1})

	cursor, err := usersCollection.Find(ctx, bson.M{}, opts)
	if err != nil {
		log.Println("Error querying users collection:", err)
		return nil, err
	}
	defer cursor.Close(ctx)

	var users []*User
	for cursor.Next(ctx) {
		var user User
		if err := cursor.Decode(&user); err != nil {
			log.Println("Error decoding user document:", err)
			continue
		}

		if user.Token == requesterToken {
			//do not return ourselves
			continue
		}

		if !isRequesterAdmin && user.Role == "admin" {
			//do not return admin user for a non admin user
			continue
		}

		users = append(users, &user)
	}

	return users, nil
}

func (db *DatabaseController) AddUser(user *AddUser) error {
	adminDatabase := db.Driver.Database("admin")
	usersCollection := adminDatabase.Collection("users")

	if db.doesUserNameExists(user.Username) {
		log.Println("User already exists. Failed to create user")
		return errors.New("user exists")
	}

	user.CreatedTime = time.Now().Unix()
	_, err := usersCollection.InsertOne(context.TODO(), user)
	if err != nil {
		log.Println(err)
		return err
	}

	log.Printf("User %s added", user.Username)
	return nil
}

func (db *DatabaseController) AddAdminUser() bool {
	adminDatabase := db.Driver.Database("admin")
	usersCollection := adminDatabase.Collection("users")

	if db.doesUserNameExists(ADMIN_USERNAME) {
		log.Println("Admin user already exist. Continuing...")
		return true
	}

	user := &AddUser{
		Username:    ADMIN_USERNAME,
		Password:    "admin",
		Name:        "Administrator",
		Role:        "admin",
		CreatedTime: time.Now().Unix(),
	}

	_, err := usersCollection.InsertOne(context.TODO(), user)
	if err != nil {
		log.Fatalf("User admin cannot be created on startup. Error %v", err)
	}

	log.Printf("User %s added", user.Username)
	return true
}

func (db *DatabaseController) UpdateUser(user *AddUser) error {
	log.Printf("Updating user: %s", user.Username)

	adminDatabase := db.Driver.Database("admin")
	usersCollection := adminDatabase.Collection("users")

	updateFields := make(map[string]interface{}, 0)
	if len(user.Password) > 0 {
		updateFields["password"] = user.Password
	}

	updateFields["name"] = user.Name
	updateFields["surname"] = user.Surname
	updateFields["role"] = user.Role

	// Define a filter to find the existing user by username
	filter := bson.D{{Key: "username", Value: user.Username}}
	update := bson.M{"$set": updateFields}

	// Perform the update operation
	_, err := usersCollection.UpdateOne(context.Background(), filter, update)
	if err != nil {
		log.Println(err)
		return err
	}

	return nil
}

func (db *DatabaseController) DeleteUser(username string) error {
	if username == ADMIN_USERNAME {
		log.Println("Deleting admin user not allowed")
		return errors.New("deleting admin user is not allowed")
	}

	adminDatabase := db.Driver.Database("admin")
	usersCollection := adminDatabase.Collection("users")

	filter := bson.M{"username": username}
	_, err := usersCollection.DeleteOne(context.TODO(), filter)

	if err != nil {
		log.Println(err)
		return err
	}

	log.Printf("User %s deleted", username)
	return err
}

// isAdmin checks if a user is an admin based on the token
func (db *DatabaseController) IsAdmin(token string) bool {
	adminDatabase := db.Driver.Database("admin")
	usersCollection := adminDatabase.Collection("users")

	// Set a timeout for the connection
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// Define a filter to find the user with the specified token
	filter := bson.M{"token": token}

	var user User
	err := usersCollection.FindOne(ctx, filter).Decode(&user)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			// User not found with the specified token
			return false
		}
		log.Println("Error querying users collection:", err)
		return false
	}

	// Check if the user has the 'admin' role
	return user.Role == "admin"
}

func (db *DatabaseController) GetCallHistory(f *Filter) (*CallInfo, error) {
	adminDatabase := db.Driver.Database("admin")
	callsCollection := adminDatabase.Collection("calls")

	// Set a timeout for the connection
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	opts := options.Find().SetSort(map[string]int{"finishedat": -1})
	if f.Limited {
		opts.SetSkip(int64(f.Offset))
		opts.SetLimit(10)
	}

	filter := bson.M{
		"finishedat": bson.M{
			"$gt": f.From,
			"$lt": f.To,
		},
	}

	searchBy := f.SearchBy
	searchBy = strings.TrimSpace(searchBy)

	if searchBy != "" {
		filter["$or"] = []bson.M{
			{"callername": bson.M{"$regex": primitive.Regex{Pattern: searchBy, Options: "i"}}},
			{"calleename": bson.M{"$regex": primitive.Regex{Pattern: searchBy, Options: "i"}}},
		}

		filter["$or"] = append(filter["$or"].([]bson.M),
			bson.M{"callid": searchBy + "\r"}, //this is a hack, fix should be when storing, but as I have data to not update this know, leave it like this
			bson.M{"callernumber": searchBy},
			bson.M{"calleenumber": searchBy},
		)
	}

	cursor, err := callsCollection.Find(ctx, filter, opts)
	if err != nil {
		log.Println("Error querying calls collection:", err)
		return nil, err
	}
	defer cursor.Close(ctx)

	var calls []*amigo.CallInfo
	for cursor.Next(ctx) {
		var call amigo.CallInfo
		if err := cursor.Decode(&call); err != nil {
			log.Println("Error decoding calls document:", err)
			return nil, err
		}

		calls = append(calls, &call)
	}

	// Count all documents in the collection as it is needed for paging
	total, err := callsCollection.CountDocuments(context.TODO(), filter)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	callInfo := &CallInfo{Total: total, Calls: calls}

	return callInfo, nil
}

func (db *DatabaseController) doesUserNameExists(username string) bool {
	var existingUser User

	adminDatabase := db.Driver.Database("admin")
	usersCollection := adminDatabase.Collection("users")

	err := usersCollection.FindOne(context.TODO(), bson.M{"username": username}).Decode(&existingUser)
	if err == mongo.ErrNoDocuments {
		// Username is not taken
		return false
	} else if err != nil {
		log.Println(err)
	}

	// Username is already taken or error happened while checking
	return true
}
