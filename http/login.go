package http

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/google/uuid"
)

type Session struct {
	Token string `json:"token"`
	Admin bool   `json:"admin"`
}

func handleUserLogin(w http.ResponseWriter, r *http.Request, db Database) {
	log.Println("Handling login request...")
	if r.Method != "POST" {
		http.Error(w, "Only POST method allowed for user login handler", http.StatusBadRequest)
		return
	}

	loginInfo := &Login{}
	if err := json.NewDecoder(r.Body).Decode(loginInfo); err != nil {
		log.Println("Failed to decode http request inside login handler")
		http.Error(w, "Failed to handle GUI login request", http.StatusBadRequest)
		return
	}

	if !db.CanLogIn(loginInfo.Username, loginInfo.Password) {
		log.Println("Failed to authorize user for login")
		http.Error(w, "Failed to authorize user for login", http.StatusBadRequest)
		return
	}

	token := uuid.NewString()
	if !db.SetUserToken(token, loginInfo.Username) {
		log.Println("Failed to set user token on login")
		http.Error(w, "Failed to authorize user for login", http.StatusInternalServerError)
		return
	}

	resp := Session{Token: token, Admin: db.IsAdmin(token)}
	respJs, _ := json.Marshal(resp)
	w.Write(respJs)
}

func handleUserLogout(w http.ResponseWriter, r *http.Request, db Database) {
	log.Println("Handling logout request...")
	if r.Method != "POST" {
		http.Error(w, "Only POST method allowed for user logout handler", http.StatusBadRequest)
		return
	}

	session := &Session{}
	if err := json.NewDecoder(r.Body).Decode(session); err != nil {
		log.Println("Failed to decode http request inside logout handler")
		http.Error(w, "Failed to handle logout request", http.StatusBadRequest)
		return
	}

	if !db.RemoveUserToken(session.Token) {
		log.Println("Failed to remove user token on logout")
		http.Error(w, "Failed to logout a user", http.StatusInternalServerError)
		return
	}
}

func handleValidateSession(w http.ResponseWriter, r *http.Request, db Database) {
	log.Println("Handling validate session request...")
	if r.Method != "POST" {
		http.Error(w, "Only POST method allowed for handle validate session handler", http.StatusBadRequest)
		return
	}

	session := &Session{}
	if err := json.NewDecoder(r.Body).Decode(session); err != nil {
		log.Println("Failed to decode http request inside validate session handler")
		http.Error(w, "Failed to handle validate session request", http.StatusBadRequest)
		return
	}

	if !db.IsSessionValid(session.Token) {
		log.Println("Session validity failed")
		http.Error(w, "User not logged in", http.StatusForbidden)
		return
	}

}
