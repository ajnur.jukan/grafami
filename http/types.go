package http

import (
	amigo "monami/amigo"

	"go.mongodb.org/mongo-driver/mongo"
)

type Database interface {
	CanLogIn(username, password string) bool
	SetUserToken(token, username string) bool
	IsSessionValid(token string) bool
	RemoveUserToken(token string) bool
	GetUsers(requesterToken string) ([]*User, error)
	DeleteUser(username string) error
	AddUser(user *AddUser) error
	UpdateUser(user *AddUser) error
	IsAdmin(token string) bool
	GetCallHistory(filters *Filter) (*CallInfo, error)
}

type DatabaseController struct {
	Driver *mongo.Client
}

type Login struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type User struct {
	Username        string `json:"username"`
	Name            string `json:"name"`
	Surname         string `json:"surname"`
	Role            string `json:"role"`
	CreatedDateTime string `json:"createdtime"`
	Token           string `json:"-"`
}

type AddUser struct {
	Username    string `json:"username"`
	Password    string `json:"password"`
	Name        string `json:"name"`
	Surname     string `json:"surname"`
	Role        string `json:"role"`
	CreatedTime int64  `json:"createdtime"`
}

type DeleteUser struct {
	Username string `json:"username"`
}

type Call struct {
	Channel string `json:"channel"`
	CallId  string `json:"callid"`
}

type CallInfo struct {
	Calls []*amigo.CallInfo
	Total int64 `json:"total"`
}

type Filter struct {
	From     int64
	To       int64
	SearchBy string
	Limited  bool
	Offset   int
}
