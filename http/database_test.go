package http

import (
	"context"
	"log"
	amigo "monami/amigo"
	"monami/database"
	"testing"
	"time"

	"github.com/go-faker/faker/v4"
	"github.com/stretchr/testify/suite"
	"go.mongodb.org/mongo-driver/bson"

	"go.mongodb.org/mongo-driver/mongo"
)

const (
	nameForUpdate          = "hasanUpdated"
	surnameForUpdate       = "hasicUpdated"
	roleForUpdate          = "unknown"
	userToken              = "test_token"
	mongoDbTestURL         = "mongodb://localhost:27018"
	numberOfCallsInHistory = 15
	userName               = "amir@gmail.com"
)

type HttpDatabaseTestSuite struct {
	suite.Suite
	db                *mongo.Client
	httpDbController  *DatabaseController
	amigoDbController *amigo.DatabaseController
	user              *AddUser
}

func TestHttpDatabaseTestSuite(t *testing.T) {
	suite.Run(t, new(HttpDatabaseTestSuite))
}

func (suite *HttpDatabaseTestSuite) SetupSuite() {
	dbClient, err := database.ConnectAndSetup(mongoDbTestURL)
	if err != nil {
		log.Fatal(err)
	}

	suite.db = dbClient
	suite.httpDbController = &DatabaseController{Driver: dbClient}
	suite.amigoDbController = &amigo.DatabaseController{Driver: dbClient}
	suite.user = getTestUser()
}

func (suite *HttpDatabaseTestSuite) TearDownSuite() {
	suite.db.Disconnect(context.TODO())
}

func (suite *HttpDatabaseTestSuite) SetupTest() {
	adminDatabase := suite.db.Database("admin")
	usersCollection := adminDatabase.Collection("users")

	// Delete all documents from the collections calls and users
	_, err := usersCollection.DeleteMany(context.TODO(), bson.D{})
	suite.Equal(nil, err)

	callsCollection := adminDatabase.Collection("calls")
	_, err = callsCollection.DeleteMany(context.TODO(), bson.D{})
	suite.Equal(nil, err)

	err = suite.httpDbController.AddUser(suite.user)
	suite.Equal(nil, err)

	user := getTestUser()
	user.Username = userName
	err = suite.httpDbController.AddUser(user)
	suite.Equal(nil, err)

	for i := 0; i < numberOfCallsInHistory; i++ {
		callInfo := &amigo.CallInfo{
			Channel:      faker.Word(),
			CallId:       faker.UUIDDigit(),
			CallerName:   faker.Name(),
			CalleeName:   faker.Name(),
			CallerNumber: faker.E164PhoneNumber(),
			CalleeNumber: faker.E164PhoneNumber(),
			StartedAt:    faker.UnixTime(),
		}

		err := suite.amigoDbController.StoreCallInfoOnHangup(callInfo)
		suite.Equal(err, nil)
	}
}

func (suite *HttpDatabaseTestSuite) TearDownTest() {
	err := suite.httpDbController.DeleteUser(suite.user.Username)
	suite.Equal(nil, err)
}

func (suite *HttpDatabaseTestSuite) TestWhenUserNameDoesNotExists() {
	exists := suite.httpDbController.doesUserNameExists("doesNotExist@gmail.com")
	suite.Equal(false, exists)
}

func (suite *HttpDatabaseTestSuite) TestAddUser() {
	exists := suite.httpDbController.doesUserNameExists(suite.user.Username)
	suite.Equal(true, exists)
}

func (suite *HttpDatabaseTestSuite) TestUpdateWithoutPasswordChangeAndGetUsers() {
	tokenSet := suite.httpDbController.SetUserToken(userToken, suite.user.Username)
	suite.Equal(true, tokenSet)

	users, err := suite.httpDbController.GetUsers(userToken)
	suite.Equal(nil, err)
	suite.NotEqual(0, len(users))

	userAlreadyExists := checkIfUserExists(users, suite.user.Username)
	suite.Equal(false, userAlreadyExists)

	suite.user.Name = nameForUpdate
	suite.user.Surname = surnameForUpdate
	suite.user.Role = roleForUpdate
	err = suite.httpDbController.UpdateUser(suite.user)
	suite.Equal(nil, err)

	users, err = suite.httpDbController.GetUsers("not_existing_token")
	suite.Equal(nil, err)
	suite.NotEqual(0, len(users))

	userAlreadyExists = checkIfUserExists(users, suite.user.Username)
	suite.Equal(true, userAlreadyExists)
}

func (suite *HttpDatabaseTestSuite) TestGetUsersWithSkippingRequester() {
	tokenSet := suite.httpDbController.SetUserToken("ampToken", userName)
	suite.Equal(true, tokenSet)

	tokenSet = suite.httpDbController.SetUserToken(userToken, suite.user.Username)
	suite.Equal(true, tokenSet)

	users, err := suite.httpDbController.GetUsers("ampToken")
	suite.Equal(nil, err)

	userExists := checkIfUserWithTokenExists(users, userToken)
	suite.Equal(true, userExists)

	users, err = suite.httpDbController.GetUsers(userToken)
	suite.Equal(nil, err)

	userExists = checkIfUserWithTokenExists(users, userToken)
	suite.Equal(false, userExists)
}

func (suite *HttpDatabaseTestSuite) TestDeleteUser() {
	err := suite.httpDbController.DeleteUser(suite.user.Username)
	suite.Equal(nil, err)
	exists := suite.httpDbController.doesUserNameExists(suite.user.Username)
	suite.Equal(false, exists)
}

func (suite *HttpDatabaseTestSuite) TestAddAndDeleteUserAdmin() {
	userAdded := suite.httpDbController.AddAdminUser()
	suite.Equal(true, userAdded)
	err := suite.httpDbController.DeleteUser(ADMIN_USERNAME)
	suite.NotEqual(nil, err)
	exists := suite.httpDbController.doesUserNameExists(ADMIN_USERNAME)
	suite.Equal(true, exists)
	userAdded = suite.httpDbController.AddAdminUser()
	suite.Equal(true, userAdded)
}

func (suite *HttpDatabaseTestSuite) TestSetAndRemoveTokenAndIsAdminUserAndIsSessionValid() {
	tokenSet := suite.httpDbController.SetUserToken(userToken, suite.user.Username)
	suite.Equal(true, tokenSet)
	isAdmin := suite.httpDbController.IsAdmin(userToken)
	suite.Equal(true, isAdmin)
	sessionValid := suite.httpDbController.IsSessionValid(userToken)
	suite.Equal(true, sessionValid)
	success := suite.httpDbController.RemoveUserToken(userToken)
	suite.Equal(true, success)
	isAdmin = suite.httpDbController.IsAdmin(userToken)
	suite.Equal(false, isAdmin)
}

func (suite *HttpDatabaseTestSuite) TestSessionValidWhenTokenIsEmpty() {
	sessionValid := suite.httpDbController.IsSessionValid("")
	suite.Equal(false, sessionValid)
}

func (suite *HttpDatabaseTestSuite) TestCanLogIn() {
	canLogin := suite.httpDbController.CanLogIn(suite.user.Username, suite.user.Password)
	suite.Equal(true, canLogin)
}

func (suite *HttpDatabaseTestSuite) TestHttpDatabaseNoConnectionTestSuite() {
	canLogin := suite.httpDbController.CanLogIn(suite.user.Username, suite.user.Password)
	suite.Equal(true, canLogin)
}

func (suite *HttpDatabaseTestSuite) TestCannotLogin() {
	canLogin := suite.httpDbController.CanLogIn("doesNotExist@gmail.com", "test123")
	suite.Equal(false, canLogin)
}

func (suite *HttpDatabaseTestSuite) TestGetCallHistoryWithoutOffset() {
	filters := &Filter{
		From:    0,
		To:      999999999999999999,
		Limited: true,
		Offset:  0,
	}

	result, err := suite.httpDbController.GetCallHistory(filters)
	suite.Equal(nil, err)
	suite.NotEqual(nil, result)

	expected := numberOfCallsInHistory
	if numberOfCallsInHistory > 10 {
		expected = 10
	}

	suite.Equal(int64(numberOfCallsInHistory), result.Total)
	suite.Equal(expected, len(result.Calls))

	orderedProperly := checkIsReturnedCallsForHistoryOrderedProperly(result.Calls)
	suite.Equal(true, orderedProperly)
}

func (suite *HttpDatabaseTestSuite) TestGetCallHistoryWithOffset() {
	filters := &Filter{
		From:    0,
		To:      999999999999999,
		Limited: true,
		Offset:  10,
	}

	result, err := suite.httpDbController.GetCallHistory(filters)
	suite.Equal(nil, err)
	suite.NotEqual(nil, result)

	expected := numberOfCallsInHistory - 10
	if expected <= 0 {
		expected = 0
	} else if expected > 10 {
		expected = 10
	}

	suite.Equal(int64(numberOfCallsInHistory), result.Total)
	suite.Equal(expected, len(result.Calls))

	orderedProperly := checkIsReturnedCallsForHistoryOrderedProperly(result.Calls)
	suite.Equal(true, orderedProperly)
}

func (suite *HttpDatabaseTestSuite) TestCsvDownloadGetCallHistoryWithoutSearch() {
	filters := &Filter{
		From:    0,
		To:      999999999999999,
		Limited: false,
		Offset:  0,
	}

	result, err := suite.httpDbController.GetCallHistory(filters)
	suite.Equal(nil, err)
	suite.NotEqual(nil, result)
	suite.Equal(int64(numberOfCallsInHistory), result.Total)
	suite.Equal(numberOfCallsInHistory, len(result.Calls))

	orderedProperly := checkIsReturnedCallsForHistoryOrderedProperly(result.Calls)
	suite.Equal(true, orderedProperly)
}

func (suite *HttpDatabaseTestSuite) TestCsvDownloadGetCallHistoryWithSearch() {
	filters := &Filter{
		From:     0,
		To:       999999999999999,
		Limited:  false,
		Offset:   0,
		SearchBy: "thisWillNotExistInDBMyAnjua!!!",
	}

	result, err := suite.httpDbController.GetCallHistory(filters)
	suite.Equal(nil, err)
	suite.NotEqual(nil, result)
	suite.Equal(int64(0), result.Total)
	suite.Equal(0, len(result.Calls))
}

func getTestUser() *AddUser {
	return &AddUser{
		Username:    "test@gmail.com",
		Password:    "test123",
		Name:        "ajnur",
		Surname:     "Jukan",
		Role:        "admin",
		CreatedTime: time.Now().Unix(),
	}
}

func checkIfUserExists(users []*User, existingUserName string) bool {
	userAlreadyExists := false
	for _, user := range users {
		if user.Username == existingUserName && user.Name == nameForUpdate && user.Surname == surnameForUpdate && roleForUpdate == user.Role {
			userAlreadyExists = true
			break
		}
	}

	return userAlreadyExists
}

func checkIsReturnedCallsForHistoryOrderedProperly(calls []*amigo.CallInfo) bool {
	// Check if the slice is empty or has only one element
	if len(calls) <= 1 {
		return true
	}

	for i := 1; i < len(calls); i++ {
		if calls[i].FinishedAt > calls[i-1].FinishedAt {
			// If the current FinishedAt is bigger the previous one, it's not ordered
			return false
		}
	}

	return true
}

func checkIfUserWithTokenExists(users []*User, token string) bool {
	for _, user := range users {
		if user.Token == userToken {
			return true
		}
	}
	return false
}
