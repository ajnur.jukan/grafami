package http

import (
	"encoding/json"
	"log"
	"net/http"
)

func handleUsersRequest(w http.ResponseWriter, r *http.Request, db Database) {
	log.Println("Handling users request...")

	switch r.Method {
	case http.MethodGet:
		token := r.URL.Query().Get("token")
		dbusers, err := db.GetUsers(token)
		if err != nil {
			http.Error(w, "Failed to get users", http.StatusInternalServerError)
			return
		}

		users, _ := json.Marshal(dbusers)
		w.Write(users)
	case http.MethodPost:
		user := &AddUser{}
		if err := json.NewDecoder(r.Body).Decode(user); err != nil {
			log.Println(err)
			http.Error(w, "Failed to handle add user request", http.StatusBadRequest)
			return
		}

		err := db.AddUser(user)
		if err != nil {
			http.Error(w, "Failed to add user", http.StatusInternalServerError)
			return
		}
	case http.MethodPatch:
		user := &AddUser{}
		if err := json.NewDecoder(r.Body).Decode(user); err != nil {
			log.Println(err)
			http.Error(w, "Failed to handle update user request", http.StatusBadRequest)
			return
		}

		err := db.UpdateUser(user)
		if err != nil {
			http.Error(w, "Failed to update user", http.StatusInternalServerError)
			return
		}
	case http.MethodDelete:
		user := &DeleteUser{}
		if err := json.NewDecoder(r.Body).Decode(user); err != nil {
			log.Println("Failed to decode http request inside delete user handler")
			http.Error(w, "Failed to handle delete user request", http.StatusBadRequest)
			return
		}

		err := db.DeleteUser(user.Username)
		if err != nil {
			http.Error(w, "Failed to delete user", http.StatusInternalServerError)
			return
		}
	default:
		http.Error(w, "Method sent is unknown", http.StatusBadRequest)
		return
	}
}
