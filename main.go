package main

import (
	"context"
	"log"
	"monami/amigo"
	"monami/database"
	monamiHttp "monami/http"
	"os"
	"os/signal"

	"syscall"
)

const mongoURL = "mongodb://localhost:27017"

var (
	httpInstance = &monamiHttp.Http{}
)

func main() {
	defer os.Exit(0)

	dbClient, err := database.ConnectAndSetup(mongoURL)
	if err != nil {
		log.Fatal(err)
	}
	defer dbClient.Disconnect(context.TODO())

	go handleOsSignal()

	amiDatabaseController := &amigo.DatabaseController{Driver: dbClient}
	ami := amigo.NewManager(amiDatabaseController)
	ami.SetConf("192.168.200.174", "5038", "ajnur", "test123")

	evChan, err := ami.Start()
	if err != nil {
		log.Fatal(err.Error())
	}
	ami.StartWS(":9999")
	ami.GetActiveCallsOnStartup()

	httpDatabaseController := &monamiHttp.DatabaseController{Driver: dbClient}
	httpInstance = monamiHttp.NewHttpInstance(httpDatabaseController, ami)
	httpInstance.InitializeHTTPRouter()
	httpInstance.StartHttp()
	defer httpInstance.StopHttp()

	httpDatabaseController.AddAdminUser()

	for event := range evChan {
		ami.EventHandler(event)
	}
}

func handleOsSignal() {
	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT,
	)

	for {
		s := <-sigc
		log.Fatalf("OS signal received %s. STOPPING MONAMI APP !", s)
	}
}
